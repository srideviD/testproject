﻿// ------------------------------------------------------------------------
// <copyright file="UnitTests.cs" company="AKQA">
//   
// </copyright>
// <summary>
//   The unit tests.
// </summary>
// ------------------------------------------------------------------------

namespace NumberToWordConversionTests
{
    using NUnit.Framework;

    using WebApplicationTest;

    /// <summary>
    /// The unit tests.
    /// </summary>
    [TestFixture]
    public class WebServiceUnitTests
    {
        /// <summary>
        /// The client.
        /// </summary>
        private readonly WebService1 client = new WebService1();

        /// <summary>
        /// The test_ action result.
        /// </summary>
        [TestCase]
        public void TestActionResult()
        {
            var expected = "John Smith\"ONE HUNDRED AND TWENTY-THREE DOLLARS AND FOURTY-FIVE CENTS\"";
            var actual = this.client.ActionResult("John Smith", 123.45);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// To test when Name is Null.
        /// </summary>
        [TestCase]
        public void TestNameNull()
        {
            var expected = "Input Name field required";
            var actual = this.client.ActionResult(" ", 123.45);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// The test to covert number to words.
        /// </summary>
        [TestCase]
        public void TestCovertNumberToWords()
        {
            var expected = "ONE HUNDRED AND TWENTY-THREE DOLLARS AND FOURTY-FIVE CENTS";
            var actual = this.client.CovertNumberToWords(123.45);

            // Assert
            Assert.AreEqual(expected, actual);
        }


    }
}























