﻿// ------------------------------------------------------------------------
// <copyright file="UnitTests.cs" company="AKQA">
//   
// </copyright>
// <summary>
//   The unit tests.
// </summary>
// ------------------------------------------------------------------------

namespace NumberToWordConversionTests
{
    using NUnit.Framework;
    using NumConverter;
    class ConverterUnitTests
    {
        /// <summary>
        /// To test for zero input.
        /// </summary>
        [TestCase]
        public void TestZero()
        {
            var expected = "ZERO DOLLARS AND ZERO CENTS";
            var actual = Converter.NumWordsWrapper(0.0);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// The test_Number to words wrapper.
        /// </summary>
        [TestCase]
        public void TestNumWordsWrapper()
        {
            string expected = "ZERO DOLLARS AND TEN CENTS";
            string actual = Converter.NumWordsWrapper(0.10);

            ////Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// To test_Number greater than thousand.
        /// </summary>
        [TestCase]
        public void TestNumGreaterThanThousand()
        {
            string expected = "ONE THOUSAND, AND EIGHTEEN DOLLARS AND ZERO CENTS";
            var actual = Converter.NumWordsWrapper(1018);

            ////Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// To test Big Number conversion.
        /// </summary>
        [TestCase]
        public void TestBigNum()
        {
            string expected = "ONE MILLION, ONE HUNDRED AND FIFTY-SIX THOUSAND, AND NINTY-NINE DOLLARS AND ZERO CENTS";
            var actual = Converter.NumWordsWrapper(1156099.0);

            ////Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// To test Large Number conversion.
        /// </summary>
        [TestCase]
        public void TestLargeNum()
        {
            string expected = "TEN TRILLION, ONE HUNDRED AND SEVEN BILLION, FOUR HUNDRED AND SIXTY-SEVEN MILLION, NINE HUNDRED AND SIXTY-FIVE THOUSAND, AND EIGHT HUNDRED AND FOURTY-THREE DOLLARS AND ZERO CENTS";
            var actual = Converter.NumWordsWrapper(10107467965843.0);

            ////Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// To test Ten Array (ie twenty thrity) conversion.
        /// </summary>
        [TestCase]
        public void TestTenArray()
        {
            string expected = "TWO THOUSAND, AND TEN DOLLARS AND ZERO CENTS";
            var actual = Converter.NumWordsWrapper(2010.0);

            ////Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// The test_Number to decimal part and rounding it two digits after decimal.
        /// </summary>
        [TestCase]
        public void TestDecimalNumberRounding()
        {
            string expected = "ZERO DOLLARS AND THIRTY-FIVE CENTS";
            string actual = Converter.NumWordsWrapper(0.347);

            ////Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// The test_Number to decimal part and rounding it two digits after decimal.
        /// </summary>
        [TestCase]
        public void TestZeroIntegerWithDecimalNumber()
        {
            string expected = "ZERO DOLLARS AND FIFTY-FOUR CENTS";
            string actual = Converter.NumWordsWrapper(0.544);

            ////Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
