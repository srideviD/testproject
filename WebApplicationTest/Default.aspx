﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplicationTest.Default" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script>
        $(document).ready(function()
        {
            $('#btnConvert').click(function()
            {
                $.ajax({
                    type: "POST",
                    contentType:"application/json; charset=utf-8",
                    dataType:'json',
                    url: '/WebService1.asmx/ActionResult',
                    data:JSON.stringify({name: $('#txtName').val(),number: $('#txtNumber').val()}),
                    success: function (data) {
                        $('.notification').html("<p>"+ data.d +"</p>");
                    },
                    error: function () {
                        $('.notification').html("");
                        alert('Please enter the required input');
                    }
                });
                return false;
            });
        });       
    </script>
    <style type="text/css">
        #txtName {
            width: 136px;
            margin-left: 14px;
        }
        #txtNumber {
            width: 138px;
        }
    </style>
</head>
<body>
    
    <form id="form1" runat="server">
    
    <div>
    <h4>Please Enter the below credentials</h4>
        <span>Enter Name:</span>
        <input type ="text" id="txtName" />
        <br />
        <br />
        <span>Enter Number:</span>
        <input type ="text" id="txtNumber" />
        <br />
        <br />
         <input type ="button" id="btnConvert" value="Num2WordConverter" />
    
        <div class="notification"></div>
    </div>
    </form>
</body>
</html>

