﻿// ------------------------------------------------------------------------
// <copyright file="WebService1.asmx.cs" company="">
//   
// </copyright>
// <summary>
//   Summary description for WebService1
// </summary>
// ------------------------------------------------------------------------

namespace WebApplicationTest
{
    using System.ComponentModel;
    using System.Text;
    using System.Web.Script.Services;
    using System.Web.Services;

    using NumConverter;
    using System;

    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]

    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [ScriptService]
    public class WebService1 : WebService
    {
        /// <summary>
        /// The action result.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="number">The number.</param>
        /// <returns>The <see cref="string"/>.</returns>
        [WebMethod]
        public string ActionResult(string name, double number)
        {
            if (!string.IsNullOrEmpty(name.Trim()))
            {
                var wordRepresentation = this.CovertNumberToWords(number);
                var stringBuilder = new StringBuilder();
                stringBuilder.Append(name).Append("\"").Append(wordRepresentation).Append("\"");
                return stringBuilder.ToString();
            }

            return "Input Name field required";
        }

        /// <summary>
        /// The covert number to words.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The <see cref="string"/>.</returns>
        [WebMethod]
        public string CovertNumberToWords(double value)
        {
            return Converter.NumWordsWrapper(value);
        }
    }
}