﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebApplicationTest
{
    public static class NumConverter
    {
        public static class constantWords
        {
            public const string glConstZero = "zero";
            public const string glConstZeroDollarCents = "zero dollars and zero cents";
            public const string glConstZeroDollars = "zero dollars";
            public const string glConstZeroCents = "zero cents";
            public const string glConstNegative = "negative ";
            public const string glConstDollars = " Dollars";
            public const string glConstCents = " Cents";

            public static string[] numbersArr = new string[] { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
            public static string[] tensArr = new string[] { "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninty" };
            public static string[] suffixesArr = new string[] { "thousand", "million", "billion", "trillion", "quadrillion", "quintillion", "sextillion", "septillion", "octillion", "nonillion", "decillion", "undecillion", "duodecillion", "tredecillion", "Quattuordecillion", "Quindecillion", "Sexdecillion", "Septdecillion", "Octodecillion", "Novemdecillion", "Vigintillion" };
        }

        public static String NumWordsWrapper(double n)
        {
            string words = string.Empty;
            double intPart;
            double decPart = 0;
            if (n == 0)
                return constantWords.glConstZeroDollarCents;
            try
            {
                string[] splitter = n.ToString().Split('.');
                intPart = double.Parse(splitter[0]);
                decPart = double.Parse(splitter[1]);
            }
            catch
            {
                intPart = n;
            }

            words = NumWords(intPart) + constantWords.glConstDollars;
            words = decPartWrapper(words, decPart);
            return words;
        }

        private static string decPartWrapper(string words, double decPart)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (decPart == 0)
            {
                if (words != string.Empty)
                    words += "  and ";
                return stringBuilder.Append(words).Append(constantWords.glConstZeroCents).ToString();
            }
            if (decPart > 0)
            {
                if (words != string.Empty)
                    words += "  and ";
                int counter = decPart.ToString().Length;
                switch (counter)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7: stringBuilder.Append(words).Append(NumWords(decPart)).Append(constantWords.glConstCents); break;
                }
            }
            return stringBuilder.ToString();
        }
        static String NumWords(double n) //converts double to words
        {
            string words = string.Empty;

            bool tens = false;

            if (n < 0)
            {
                words += constantWords.glConstNegative;
                n *= -1;
            }

            int power = (constantWords.suffixesArr.Length + 1) * 3;

            while (power > 3)
            {
                double pow = Math.Pow(10, power);
                if (n >= pow)
                {
                    if (n % pow > 0)
                    {
                        words += NumWords(Math.Floor(n / pow)) + " " + constantWords.suffixesArr[(power / 3) - 1] + ", ";
                    }
                    else if (n % pow == 0)
                    {
                        words += NumWords(Math.Floor(n / pow)) + " " + constantWords.suffixesArr[(power / 3) - 1];
                    }
                    n %= pow;
                }
                power -= 3;
            }
            if (n >= 1000)
            {
                if (n % 1000 > 0) words += NumWords(Math.Floor(n / 1000)) + " thousand, ";
                else words += NumWords(Math.Floor(n / 1000)) + " thousand";
                n %= 1000;
            }
            if (0 <= n && n <= 999)
            {
                if ((int)n / 100 > 0)
                {
                    words += NumWords(Math.Floor(n / 100)) + " hundred";
                    n %= 100;
                }
                if ((int)n / 10 > 1)
                {
                    if (words != string.Empty)
                        words += string.Empty;
                    words += constantWords.tensArr[(int)n / 10 - 2];
                    tens = true;
                    n %= 10;
                }

                if (n < 20 && n > 0)
                {
                    if (words != string.Empty && tens == false)
                        words += string.Empty;
                    words += (tens ? "-" + constantWords.numbersArr[(int)n - 1] : constantWords.numbersArr[(int)n - 1]);
                    n -= Math.Floor(n);
                }

            }
            return words;
        }
    }
}