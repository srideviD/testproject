﻿// ------------------------------------------------------------------------
// <copyright file="Default.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The default.
// </summary>
// ------------------------------------------------------------------------

namespace WebApplicationTest
{
    using System;
    using System.Web.UI;

    /// <summary>
    /// The default.
    /// </summary>
    public partial class Default : Page
    {
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}