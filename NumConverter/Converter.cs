﻿// ------------------------------------------------------------------------
// <copyright file="Converter.cs" company="AKQA">
//  
// </copyright>
// <summary>
//   The converter.
// </summary>
// ------------------------------------------------------------------------

namespace NumConverter
{
    using System;
    using System.Text;

    /// <summary>
    /// The converter.
    /// </summary>
    public static class Converter
    {
        /// <summary>
        /// The num words wrapper.
        /// </summary>
        /// <param name="n">The n.</param>
        /// <returns>The <see cref="string"/>.</returns>
        public static string NumWordsWrapper(double n)
        {
            var words = string.Empty;
            double intPart;
            double decPart = 0;
            if (n == 0) return ConstantWords.ConstZeroDollarCents.ToUpper();

            try
            {
                string[] splitter = n.ToString("N2").Split('.');
                intPart = double.Parse(splitter[0]);
                decPart = double.Parse(splitter[1]);
            }
            catch
            {
                intPart = n;
            }

            words = NumWords(intPart) + ConstantWords.ConstDollars;
            words = DecPartWrapper(words, decPart);
            return words.ToUpper();
        }

        /// <summary>
        /// The dec part wrapper.
        /// </summary>
        /// <param name="words">The words.</param>
        /// <param name="decPart">The decimal part.</param>
        /// <returns>The <see cref="string"/>.</returns>
        private static string DecPartWrapper(string words, double decPart)
        {
            var stringBuilder = new StringBuilder();
            if (decPart == 0)
            {
                if (words != string.Empty) words += " and ";
                return stringBuilder.Append(words).Append(ConstantWords.ConstZeroCents).ToString();
            }

            if (decPart > 0)
            {
                if (words != string.Empty) words += " and ";
                var counter = decPart.ToString().Length;
                switch (counter)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        stringBuilder.Append(words).Append(NumWords(decPart)).Append(ConstantWords.ConstCents);
                        break;
                }
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// The num words.
        /// </summary>
        /// <param name="n">The n.</param>
        /// <returns>The <see cref="string"/>.</returns>
        private static string NumWords(double n)
        {
            // converts double to words
            var words = string.Empty;
            if (n == 0)
                return ConstantWords.ConstZero;
            var tens = false;

            if (n < 0)
            {
                words += ConstantWords.ConstNegative;
                n *= -1;
            }

            var power = (ConstantWords.SuffixesArr.Length + 1) * 3;

            while (power > 3)
            {
                var pow = Math.Pow(10, power);
                if (n >= pow)
                {
                    if (n % pow > 0)
                    {
                        words += NumWords(Math.Floor(n / pow)) + " " + ConstantWords.SuffixesArr[power / 3 - 1] + ", ";
                    }
                    else if (n % pow == 0)
                    {
                        words += NumWords(Math.Floor(n / pow)) + " " + ConstantWords.SuffixesArr[power / 3 - 1];
                    }

                    n %= pow;
                }

                power -= 3;
            }

            if (n >= 1000)
            {
                if (n % 1000 > 0) words += NumWords(Math.Floor(n / 1000)) + " thousand, and ";
                else words += NumWords(Math.Floor(n / 1000)) + " thousand and";
                n %= 1000;
            }

            if (0 <= n && n <= 999)
            {
                if ((int)n / 100 > 0)
                {
                    words += NumWords(Math.Floor(n / 100)) + " hundred and ";
                    n %= 100;
                }

                if ((int)n / 10 > 1)
                {
                    if (words != string.Empty) words += string.Empty;
                    words += ConstantWords.TensArr[(int)n / 10 - 2];
                    tens = true;
                    n %= 10;
                }

                if (n < 20 && n > 0)
                {
                    if (words != string.Empty && tens == false) words += string.Empty;
                    words += tens ? "-" + ConstantWords.NumbersArr[(int)n - 1] : ConstantWords.NumbersArr[(int)n - 1];
                    n -= Math.Floor(n);
                }
            }

            return words;
        }

        /// <summary>
        /// The constant words.
        /// </summary>
        private static class ConstantWords
        {
            /// <summary>
            /// The const cents.
            /// </summary>
            public const string ConstCents = " Cents";

            /// <summary>
            /// The const dollars.
            /// </summary>
            public const string ConstDollars = " Dollars";

            /// <summary>
            /// The const negative.
            /// </summary>
            public const string ConstNegative = "negative ";

            /// <summary>
            /// The const zero.
            /// </summary>
            public const string ConstZero = "zero";

            /// <summary>
            /// The const zero cents.
            /// </summary>
            public const string ConstZeroCents = "zero cents";

            /// <summary>
            /// The const zero dollar cents.
            /// </summary>
            public const string ConstZeroDollarCents = "zero dollars and zero cents";

            /// <summary>
            /// The const zero dollars.
            /// </summary>
            public const string ConstZeroDollars = "zero dollars";

            /// <summary>
            /// The numbers arr.
            /// </summary>
            public static readonly string[] NumbersArr =
                {
                    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen",
                    "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"
                };

            /// <summary>
            /// The suffixes arr.
            /// </summary>
            public static readonly string[] SuffixesArr =
                {
                    "thousand", "million", "billion", "trillion", "quadrillion", "quintillion", "sextillion", "septillion", "octillion",
                    "nonillion", "decillion", "undecillion", "duodecillion", "tredecillion", "Quattuordecillion", "Quindecillion",
                    "Sexdecillion", "Septdecillion", "Octodecillion", "Novemdecillion", "Vigintillion"
                };

            /// <summary>
            /// The tens arr.
            /// </summary>
            public static readonly string[] TensArr = { "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninty" };
        }
    }
}